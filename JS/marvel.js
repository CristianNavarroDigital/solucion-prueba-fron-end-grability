var marvel = 
{
	render: function ()
	{
		/* url utilizada para consumir el api de MArvel donde se encuentra toda la informacion de los personajes
		-	
		-
		Con los datos que se dan en la pagina de mavel el public key y el provate key se genera un md5 hash qpara utilizar en la construccion del url como lo indicala documentacion de marvel.

		*/
		var url = "http://gateway.marvel.com/v1/public/characters?ts=1&apikey=87e84e2b182bccc4edee0960bf6f07f5&hash=fa4c749edf55f1d5cc7a045147fe3135";

		/* declaracion de variables que comunican la informacion de Api con la interfaz del HTML con el Id podemos asgnar datos a espacion especificos en el HTML.*/
		
		var message = document.getElementById("message");
		var footer = document.getElementById("footer");
		var marvelContainer = document.getElementById("marvel-container");
		
	
		$.ajax({
			url: url,
			type: "GET",
			beforeSend: function()
			{
				message.innerHTML = "Loading...";
			},
			complete: function()
			{
				message.innerHTML = "Well Done!";
			},
			success: function(data)
			{
				footer.innerHTML = data.attributionHTML;
				var string = "";
				string += "<div class='row'>";
				/* con le for evitamos generar un codigo HTML tan extenso generando un ciclo de verificacion de la informacion llenando campos especificos de los datos solicitados en el API
				-
				-
				-
				Por medio de la asiganciond e una variable vacia, declaramos la creacion de diveras lineas de codigo en el HTML desde el JS, utilizando la estructura de los links recomendados en Marvel, hacemos llamados de diversos datos del JSON que nos proporciona el API.
				*/
				for (var i = 0; i<data.data.results.length; i++)
				{
					var element = data.data.results [i];
					

					string += "<div class='col-sm-6'> <div class='cuadro col-sm-11'>";
					string += "<div class='col-sm-6'><img class='imagen img-circle' src='"+ element.thumbnail.path +"/standard_fantastic."+element.thumbnail.extension+"' /> </div>";
					string += "<div class='col-sm-6 descripcion'><h3>" + element.name + "</h3>";
					string += "<div class='descripcionC'><p>" + element.description + "</p></div>";
					string += "<div class='col-sm-10'><a class='linkMore' href='" + element.urls[0].url +"' target='_blank'> <strong>VIEW MORE</strong></a> </div> </div>";
					string += "<div class='col-sm-12'><h4 class='text-muted'> Related Comics</h4> </div>";
					string += "</div></div>";

					if ((i+1) % 2 == 0)
					{
						string += "</div>";
						string += "<div class='row'>";
					}
				}

				
				marvelContainer.innerHTML = string;
				
				
			},
			error: function()
			{
				message.innerHTML = "Sorry, We dont Reach It!"
			}
		});

	}
};
marvel.render();
/* prueba para hacer el llamado de los comis para la seccion de favoritos**********************************************

var comics =
 {
 	render1: function ()
 	{
 		var url = "http://gateway.marvel.com/v1/public/comics?ts=1&apikey=87e84e2b182bccc4edee0960bf6f07f5&hash=fa4c749edf55f1d5cc7a045147fe3135";

 		var comicContainer = document.getElementById("comic-container");

 		$ajax({
 			url: url,
			type: "GET",

 			success: function(data)
 			{
 				var string2 = "";

				string2 += "<div class='row'>";

				for (var j = 0; j<data.data.results.length; j++)
				{
					var element = data.data.results [j];

					string += "<div class='col-sm-12'><img class='imagen img-circle' src='"+ element.thumbnail.path +"/portrait_fantastic."+element.thumbnail.extension+"' /> </div>";
					string += "<div class='col-sm-12 descripcion'><h3>" + element.title + "</h3>";

					if ((j+1) % 1 == 0)
					{
						string += "</div>";
						string += "<div class='row'>";
					}
				}

				comicContainer.innerHTML = string2;
			}
 		});
 	}

 }
 comics.render1();*/